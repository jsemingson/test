/*
Modified By: Ralph Callaway <ralph@callawaycloudconsulting.com>
*/
global class testClass
{
    static testMethod void projectUpd() 
    {
/*
        Account acctrec = [select Id, Estimated_Contract_Complete_Date__c from Account where Id = '0015000000MWOnD'];
        acctrec.Estimated_Contract_Complete_Date__c = Date.today();
        update acctrec;
        
        Opportunity oppRec = [select Id from Opportunity where Id = '0065000000BTvZ5'];
        oppRec.Project_Manager_Comments__c = 'PM Comments- by Raju';
        oppRec.Legal_Comments__c = 'Legal comments- by Raju';
        update oppRec;
        
        Opportunity oppRec2 = [select Id from Opportunity where Id = '0065000000BTvZ5'];
        oppRec2.Project_Manager_Comments__c = 'PM Comments- by Raju';
        oppRec2.Legal_Comments__c = 'Legal comments- by Raju';
        update oppRec2;
*/
        Account acctrec = TestUtil.createAccount();
        acctrec.Estimated_Contract_Complete_Date__c = Date.today();
        update acctrec;
        
        Opportunity oppRec = TestUtil.createOpportunity(acctrec.id);
        oppRec.Project_Manager_Comments__c = 'PM Comments- by Raju';
        oppRec.Legal_Comments__c = 'Legal comments- by Raju';
        update oppRec;
        
        Opportunity oppRec2 = oppRec.clone(true, true);
        oppRec2.Project_Manager_Comments__c = 'PM Comments- by Raju';
        oppRec2.Legal_Comments__c = 'Legal comments- by Raju';
        update oppRec2;

        tenrox__c rec = TestUtil.createProject();
        rec.Status_Comments__c = 'Project status comment-Raju';
        rec.Number_of_VCAS_SMC_Servers__c = 10;
        rec.Number_of_RTES_Servers__c = 10;
        rec.Number_of_VOD_Encryption_Servers__c = 10;
        rec.opportunity__c = oppRec.Id;
        rec.Forecast_Project_Close_Acceptance_Date__c = Date.today();
        rec.Number_of_Dedicated_Database_Servers__c = 10;
        rec.Number_of_STBs__c = 10;
        update rec;

        tenrox__c[] newProjRec = new tenrox__c[1];
        newProjRec[0] = TestUtil.generateProject();
        newProjRec[0].Status_Comments__c = 'Project status comment';
        newProjRec[0].Number_of_VCAS_SMC_Servers__c = 10;
        newProjRec[0].Number_of_RTES_Servers__c = 10;
        newProjRec[0].Number_of_VOD_Encryption_Servers__c = 10;
//      newProjRec[0].opportunity__c = '0065000000BTvZ5';
//      newProjRec[0].Account__c =  '0015000000Hs3M2';
        newProjRec[0].opportunity__c = oppRec.Id;
        newProjRec[0].Account__c =  acctrec.Id;
        newProjRec[0].Forecast_Project_Close_Acceptance_Date__c = Date.today();
        insert newProjRec;

//No Schedule exists scenario - With a project header Update And delivery record insert - Should create a New Schedule - Forecast Date in schedule shud be empty
        
        tenrox__c projRec = rec.clone(true, true);
 
        projRec.Forecast_Project_Close_Acceptance_Date__c = Date.today();
        update projRec;

        Project_Deliverable__c projDelvRec1 = new Project_Deliverable__c();
        projDelvRec1.Project__c = projRec.Id;
        projDelvRec1.Amount__c = 2000.00;
        insert projDelvRec1;        

//Single Schedule Scenario - With a project header Update And delivery record insert - Should Not Update the Date in the schedule, Schedule Amount should be rolled up from deliverable

        tenrox__c projRec1 = rec.clone(true, true);
         projRec1.Forecast_Project_Close_Acceptance_Date__c = Date.today();
        update projRec1;

        Project_Deliverable__c projDelvRec = new Project_Deliverable__c();
        projDelvRec.Project__c = projRec.Id;
        projDelvRec.Amount__c = 1000.00;
        insert projDelvRec;

//Multiple Schedule Scenario - With a project header Update And delivery record insert - Should Not Do Anything
        
        tenrox__c projRec2 = rec.clone(true, true);
        projRec2.Forecast_Project_Close_Acceptance_Date__c = Date.today();
        update projRec2;
        
        Project_Deliverable__c projDelvRec2 = new Project_Deliverable__c();
        projDelvRec2.Project__c = projRec.Id;
        projDelvRec2.Amount__c = 1000.00;
        insert projDelvRec2; 
        
        update projDelvRec2; 
        
        delete projDelvRec2;
        
               
    }
    
    static testMethod void attachTest() 
    {
    	Account testAccount = TestUtil.createAccount();
    	
 // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();

        email.subject = 'Test Class';
        email.fromName = 'test test';
        email.fromAddress = 'rramaswamy@demandsolutionsgroup.com';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only. Bye. Account Reference [DO NOT MODIFY]: ' + testAccount.id;

        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        
        // setup controller object
        attachmentCatcher catcher = new attachmentCatcher();
        catcher.handleInboundEmail(email, envelope);      
        
        Messaging.InboundEmail.Textattachment[] textAttachs = new Messaging.InboundEmail.Textattachment[1];  
        Messaging.InboundEmail.Textattachment textAttach = new Messaging.InboundEmail.Textattachment();

        textAttach.fileName = 'test.txt';
        textAttach.body = 'Test Test Test Test Test Test Test Test Test';
        textAttachs[0] = textAttach;
        email.textAttachments = textAttachs;
         // setup controller object
        catcher = new attachmentCatcher();
        catcher.handleInboundEmail(email, envelope);
                  
    }
 
    static testMethod void oppTest()
    {
        User userRec1 = TestUtil.generateUser();
        User userRec2 = TestUtil.generateUser();
        User userRec3 = TestUtil.generateUser();
        User userRec4 = TestUtil.generateUser();
        User userRec5 = TestUtil.generateUser();
        insert new User[] { userRec1, userRec2, userRec3, userRec4, userRec5 };
        
        // RALPH: modified to satisfy customer main opportunity validation rule
        Account testAccount = TestUtil.createAccount();
        Opportunity testOppty = TestUtil.createOpportunity(testAccount.id);
        Opportunity oppRec = TestUtil.generateOpportunity(testAccount.id);
        oppRec.Customer_main_Opportunity__c = testOppty.id;
        oppRec.operator_type__c = TestUtil.TEST_STRING;
        oppRec.VCAS_Solution__c = TestUtil.TEST_STRING;
        insert oppRec;
        
        oppRec.PO_Currency__c = TestUtil.TEST_STRING;
        oppRec.Prepayment_Required__c = TestUtil.TEST_STRING;
        oppRec.Intercompany_Transaction__c = true;
        oppRec.Verimatrix_Entity__c = TestUtil.TEST_STRING;
        oppRec.Purchase_Order_Date__c = system.today();
        oppRec.Purchase_Order__c = TestUtil.TEST_STRING;
        oppRec.StageName = 'Closed Won';
        oppRec.Salesperson_1__c = userRec1.Id;
        oppRec.Percent_Split_1__c = '20.00';

        oppRec.Salesperson_2__c = userRec2.Id;
        oppRec.Percent_Split_2__c = '20.00';

        oppRec.Salesperson_3__c = userRec3.Id;
        oppRec.Percent_Split_3__c = '20.00';

        oppRec.Salesperson_4__c = userRec4.Id;
        oppRec.Percent_Split_4__c = '20.00';

        oppRec.Salesperson_5__c = userRec5.Id;
        oppRec.Percent_Split_5__c = '20.00';
        Update oppRec;

        Opportunity oppRec2 = TestUtil.createOpportunity(null);
        Update oppRec2;
    }

    static testMethod void invoiceTest()
    {
        Date paidDate = date.today();
   
        Opportunity oppRec = TestUtil.createOpportunity(null);
        
        AVSFQB__Invoices__c invoiceRec = new AVSFQB__Invoices__c();
        invoiceRec.AVSFQB__Opportunity__c = oppRec.Id;
        invoiceRec.AVSFQB__Total_Invoice_Amount__c = 1000.00;
        invoiceRec.AVSFQB__Payments__c = 500.00;
        invoiceRec.Paid_Date__c = paidDate;
        
        insert invoiceRec;
        
        invoiceRec.AVSFQB__Payments__c = 700.00;
        
        update invoiceRec;
        
        delete invoiceRec;
    }
    
}