global class TagIndexerBatchUpdateTagIndex implements Database.Batchable<sObject>{

	global Database.QueryLocator start(Database.BatchableContext BC) {
		// pass in all latest content versions
		return Database.getQueryLocator([select contentDocumentId, tagCsv from ContentVersion where isLatest = true]);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> batch) {
		TagIndexerCore.updateTagIndex(batch);
	}

	global void finish(Database.BatchableContext BC) { }

	@isTest
	private static void basicTagIndexerBatchUpdateTagIndexTest() {
		Test.startTest();
		TagIndexerBatchUpdateTagIndex batchClass = new TagIndexerBatchUpdateTagIndex();
		Database.executeBatch(batchClass);
		Test.stopTest();
	}
}