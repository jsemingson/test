public with sharing class RecentContent {

	public Integer contentCount { get; set; }
	
	public List<ContentWrapper> recentContent {
		get {
			if(recentContent == null) {
				// query most recent publish events
				List<ContentDocumentHistory> recentDocumentUploads = 
					[select contentDocumentId, createdDate from ContentDocumentHistory 
					 where field='contentDocPublished' order by createdDate desc limit :contentCount];
				
				// query the content documents for the recent uploads
				Set<Id> recentContentIds = new Set<Id>();
				for(ContentDocumentHistory recentDocumentUpload : recentDocumentUploads) {
					recentContentIds.add(recentDocumentUpload.contentDocumentId);
				}
				Map<Id, ContentDocument> contentMap = new Map<Id, ContentDocument>(
					[select title from ContentDocument where id in :recentContentIds]);
				
				// wrap content with it's publication date			
				recentContent = new List<ContentWrapper>();
				for(ContentDocumentHistory recentDocumentUpload : recentDocumentUploads) {
					ContentWrapper contentWrapper = new ContentWrapper();
					contentWrapper.content = contentMap.get(recentDocumentUpload.contentDocumentId);
					contentWrapper.publicationDate = recentDocumentUpload.createdDate;
					recentContent.add(contentWrapper);
				}
			}
			return recentContent;
		}
		private set; 
	}

	public class ContentWrapper {
		public ContentDocument content { get; set; }
		public DateTime publicationDate { get; set; }
	}
	
	@isTest
	private static void basicRecentContentTest() {
		RecentContent controller = new RecentContent();
		controller.contentCount = 5;
		List<ContentWrapper> recentContent = controller.recentContent;
	}

}