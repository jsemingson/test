/*
     Created By Ralph Callaway
     Description:
       Place all data creation utility functions in this class so they can be shared
       across multiple tests.  That way if new validation rules break a test you only
       have to make a change in one place
       
       Modifiled by: Anil Sahoo
       
       test class modified to to accommodate the new validation rules.
*/
public class TestUtil {

    public static String TEST_STRING = generateRandomString();
    public static String RESELLER_NAME = generateRandomString();
    public static Date TEST_DATE = System.today();

    /* Standard Object Functions */

    // Accounts
    public static Account createAccount() {
        Account account = generateAccount();
        insert account;
        return account;
    }
    public static Account generateAccount() {
        return new Account(name = TEST_STRING, billingCountry = 'Qatar', shippingCountry = 'Qatar'); 
        
    }

    // Reseller
    public static Account getReseller() {
    
        Account reseller = [Select Id, name from Account where type = 'Partner' limit 1];
        if (reseller == null) {
            reseller = new Account(name = RESELLER_NAME, billingCountry = 'Qatar', shippingCountry = 'Qatar', type='Partner');
            insert reseller;
        }
        return reseller;
    }
    
    
    // Case
    public static Case createCase() {
        Case testCase = generateCase();
        insert testCase;
        return testCase;
    }
    public static Case generateCase() {
        return new Case(subject = TEST_STRING, description = TEST_STRING);
    }
    
    // Contacts
    public static Contact createContact(Id accountId) {
        Contact contact = generateContact(accountId);
        insert contact;
        return contact;
    }
    public static Contact generateContact(Id accountId) {
        return new Contact(lastName = TEST_STRING, mailingCountry = 'Qatar', accountId = accountId);
    }
    
    // Leads
    public static Lead createLead() {
        Lead lead = generateLead();
        insert lead;
        return lead;
    }
    public static Lead generateLead() {
        return new Lead(lastName = TEST_STRING, company = TEST_STRING, country= TEST_STRING);
    }
     
    // Opportunity
    public static Opportunity createOpportunity(Id accountId) {
        Opportunity opportunity = generateOpportunity(accountId);
        insert opportunity;
        return opportunity;
    }
    public static Opportunity generateOpportunity(Id accountId) {
        
        Account account =[Select Id, name, billingCountry from Account limit 1];
        Account reseller = getReseller();
        //Account account = [Select Id, name from Account where id = :accountId];
        return new Opportunity(
              closeDate = TEST_DATE,
              name = account.name,
              reseller_name__c = reseller.name,
              // reseller_name__c = TEST_STRING,
              stageName = TEST_STRING,
              Delivery_Country__c = account.billingCountry,
              accountId = accountId,
              Reseller__c = reseller.Id,
              //Reseller__c = testAcc.Id,
              PMO_Required__c = 'YES',
              PMO_Required_Reason__c = 'Other'
        );
    }

    // User
    public static User createUser() {
        User testUser = generateUser();
        insert testUser;
        return testUser;
    }
    public static User generateUser() {
        String testString = generateRandomString(8);
        String testEmail = generateRandomEmail();
        return new User(lastName = testString,
            userName = testEmail,
            profileId = SYSADMIN_PROFILE_ID,
            alias = testString,
            email = testEmail,
            emailEncodingKey = 'ISO-8859-1',
            languageLocaleKey = 'en_US',
            localeSidKey = 'en_US',
            timeZoneSidKey = 'America/Los_Angeles'
        );
    }
    
    /* Custom Objects */
    
    // M&S Milestone
    public static M_S_Milestone__c createMandSMilestone(Id accountId) {
        M_S_Milestone__c milestone = generateMandSMilestone(accountId);
        insert milestone;
        return milestone;
    }
    public static M_S_Milestone__c generateMandSMilestone(Id accountId) {
        return new M_S_Milestone__c(
              account__c = accountId
            , start_date__c = TEST_DATE
            , po_number__c = TEST_STRING
            , customer__c = TEST_STRING
        );
    }
    
    // Project
    public static tenrox__c createProject() {
        tenrox__c project = generateProject();
        insert project;
        return project;
    }
    public static tenrox__c generateProject() {
        return new tenrox__c(project_owner__c = TEST_ADMIN_ID);
    }
    
    // Quote
    public static SFDC_520_Quote__c createQuote(Id opportunityId) {
        SFDC_520_Quote__c quote = generateQuote(opportunityId);
        insert quote;
        return quote;
    }
    public static SFDC_520_Quote__c generateQuote(Id opportunityId) {
        return new SFDC_520_Quote__c(opportunity__c = opportunityId);
    }

    /* Memoized Properties */

    public static Id SYSADMIN_PROFILE_ID
    {
        get {
            if(null == SYSADMIN_PROFILE_ID) {
                SYSADMIN_PROFILE_ID = [select id from Profile where name = 'System Administrator'][0].id;
            }
            return SYSADMIN_PROFILE_ID; 
        }
        set;
    }
    
    public static Id TEST_ADMIN_ID
    {
        get {
            if(null == TEST_ADMIN_ID) {
                TEST_ADMIN_ID = createUser().id;    
            }
            return TEST_ADMIN_ID;
        }
        set;
    }
        
    /* Random Functions */

    public static String generateRandomString(){return generateRandomString(null);}
    public static String generateRandomString(Integer length){
        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        return returnString;
    }
    public static String generateRandomEmail(){return generateRandomEmail(null);}
    public static String generateRandomEmail(String domain){
        if(domain == null || domain == '')
            domain = generateRandomString() + '.com';
        return generateRandomString() + '@' + domain;
    }

    public static String generateRandomUrl() {
        return 'http://' + generateRandomString() + '.com'; 
    }

    /* Test Functions */

    @isTest
    private static void testStandardObjects() {
        User testUser = createUser();
        Case testCase = createCase();
        Lead testLead = createLead();
        Account testAccount = createAccount();
        Contact testContact = createContact(testAccount.id);
        Opportunity testOpportunity = createOpportunity(testAccount.id);
    }
    
    @isTest
    private static void testCustomObjects() {
        Account testAccount = createAccount();
        Opportunity testOpportunity = createOpportunity(testAccount.id);
        tenrox__c testProject = createProject();
        M_S_Milestone__c testMilestone = createMandSMilestone(testAccount.id);
        SFDC_520_Quote__c testQuote = createQuote(testOpportunity.id);
    }

    @isTest
    private static void testRandomFunctions() {
        String randomString = generateRandomString();
        String randomEmail = generateRandomEmail();
        String randomUrl = generateRandomUrl();
    }

}