@istest(seeAllData= true)
public class TestRegenUntilSuccess{
//test method for BatchLicenseGeneratorOnSystem
public static testMethod void testBatchLicenseGenerator()
{
     Test.StartTest();
     System__c s= [select ID from System__c Order by LastModifiedDate Desc Limit 1  ];
     //created a list of 13 servers to test the batch license generator and regenerator. the number can be changed to any value    
     list <Server__c> li= [Select Id, Name, Server_License_Key__c ,Success_Error_Code__c,License_Start_Date__c,Inactive__c  from Server__c Order By LastModifiedDate Desc limit 13];
     RegenUntilSuccess r = new RegenUntilSuccess(s.ID,li,0,3);
     ID bpid = Database.executeBatch(r);
     Test.StopTest();
     System.AssertEquals(10,10);
}

}