public class TagIndexerCore {
	
	public static void updateTagIndex(List<sObject> genericInput) {		
		
		// cast input to content version
		List<ContentVersion> versions = castGenericInput(genericInput);
		
		// collect related document ids and version ids
		Set<Id> documentIds = new Set<Id>();
		Set<Id> versionIds = new Set<Id>();
		for(ContentVersion version : versions) {
			documentIds.add(version.contentDocumentId);
			versionIds.add(version.id);
		}
	
		// create map of tag sets for each document
		Map<Id, Set<String>> tagSetMap = new Map<Id, Set<String>>();
		for(ContentVersion version : versions) {
			Set<String> tags = new Set<String>();
			if(null != version.TagCsv)
				tags.addAll(version.TagCsv.split(','));
			tagSetMap.put(version.id, tags);
		}
		
		// create a map of workspace sets for each document
		Map<Id, Set<Id>> workspaceSetMap = new Map<Id, Set<Id>>();
		for(ContentWorkspaceDoc workspaceJoin : 
		 [select contentWorkspaceId, contentDocumentId from ContentWorkspaceDoc 
		  where contentDocumentId in :documentIds]) {
			if(!workspaceSetMap.containsKey(workspaceJoin.contentDocumentId))
				workspaceSetMap.put(workspaceJoin.contentDocumentId, new Set<Id>());
			workspaceSetMap.get(workspaceJoin.contentDocumentId).add(workspaceJoin.contentWorkspaceId);
		}
		
		// clear out old tags
		for(List<TagIndex__c> bucket : bucketize([select id from TagIndex__c where versionId__c in :versionIds])) {
			delete bucket;
		}
		
		// create new tags
		List<TagIndex__c> newTags = new List<TagIndex__c>(); 
		for(ContentVersion version : versions) {
			Set<String> tagSet = tagSetMap.get(version.id);
			String workspaceString = setToMultiSelectString(workspaceSetMap.get(version.contentDocumentId));
			for(String tag : tagSet) {
				newTags.add(new TagIndex__c(name = tag, versionId__c = version.id, workspaceIds__c = workspaceString));
			}
		}
		for(List<TagIndex__c> bucket : bucketize(newTags)) {
			insert bucket;
		}
	}
	
	public static void removeOldVersionsFromIndex(List<SObject> genericInput) {
		
		// cast input to content version
		List<ContentVersion> versions = castGenericInput(genericInput);
		
		// collect old version ids
		Set<String> oldVersionIds = new Set<String>();
		for(ContentVersion version : versions) {
			oldVersionIds.add(version.id);
		}
		
		// query and delete all related indices
		List<TagIndex__c> oldTagIndices = [select id from TagIndex__c where versionId__c in :oldVersionIds];
		for(List<TagIndex__c> tagBatch : bucketize(oldTagIndices)) {
			delete tagBatch;
		}
	}
	
	// cast generic input to content version
	private static List<ContentVersion> castGenericInput(List<SObject> genericInput) {
		List<ContentVersion> versions = new List<ContentVersion>();
		for(SObject generic : genericInput) {
			versions.add((ContentVersion) generic);
		}
		return versions;
	}
	
	// take a set and convert it into a multiselect string
	private static String setToMultiSelectString(Set<Id> input) {
		if(null == input || input.isEmpty())
			return '';
		
		String output = '';
		for(Id value : input) {
			output += ';' + value;
		}
		output = output.subString(1);
		return output;
	}
	
	// break a set of large list of tags up into the chunks that can be passed to the database
	private static Integer MAX_DML_BATCH = 1000;
	private static List<List<TagIndex__c>> bucketize(List<TagIndex__c> input) {
		List<List<TagIndex__c>> buckets = new List<List<TagIndex__c>>();
		List<TagIndex__c> bucket = new List<TagIndex__c>();
		for(TagIndex__c tag : input) {
			bucket.add(tag);
			if(bucket.size() == MAX_DML_BATCH) {
				buckets.add(bucket.clone());
				bucket = new List<TagIndex__c>();
			}
		}
		if(!bucket.isEmpty())
			buckets.add(bucket);
		return buckets;
	}

}