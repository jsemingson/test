Global class ServerIterable implements Iterator<Server__c>{
    
      List<Server__c> sys {get; set;} 
   Integer i {get; set;} 
  
   public ServerIterable(List<Server__c> li)
   {
     sys=li;
     i=0;
     System.debug(sys);
     System.debug(li);
   }
   global boolean hasNext(){ 
       if(i >= sys.size()) 
           return false; 
       else 
           return true; 
   }    
   global Server__c next(){ 
       if(i == sys.size()){ i++; return null;} 
       i=i+1; 
       return sys[i-1]; 
   } 
}