/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Consolidated location for simple OBJECT trigger logic.  For more complex object a separate class should be created.
*/
public class Project_TriggerHelper {

	/* Trigger Methods */

	// When a project is created query the line items and pull them down to the project as deliverables
	public static void createProjectDeliverables(List<tenrox__c> projects) {
		// query line items for related opportunities
		Set<Id> opptyIds = new Set<Id>();
		Map<Id, List<OpportunityLineItem>> lineItemMap = new Map<Id, List<OpportunityLineItem>>();
		for(tenrox__c project : projects) {
			opptyIds.add(project.opportunity__c);
		}
		for(OpportunityLineItem lineItem : [
			select 
				  description
				, opportunityId
				, pricebookEntry.product2Id
				, quantity
				, totalPrice
			from OpportunityLineItem
			where opportunityId in :opptyIds  
		]) {
			if(!lineItemMap.containsKey(lineItem.opportunityId)) {
				lineItemMap.put(lineItem.opportunityId, new List<OpportunityLineItem>());
			}
			lineItemMap.get(lineItem.opportunityId).add(lineItem);
		}
		
		// create project deliverables
		List<Project_Deliverable__c> newDeliverables = new List<Project_Deliverable__c>();
		for(tenrox__c project : projects) {
			if(lineItemMap.containsKey(project.opportunity__c)) {
				for(OpportunityLineItem lineItem : lineItemMap.get(project.opportunity__c)) {
					newDeliverables.add(new Project_Deliverable__c(
						  amount__c = lineItem.totalPrice
						, customer_main_project__c = project.Customer_main_Project__c
						, line_description__c = lineItem.description
						, product__c = lineItem.pricebookEntry.product2Id
						, project__c = project.id
						, quantity__c = lineItem.quantity
					));
				}
			}
		}
		insert newDeliverables;
	}

	/* Test Methods */
	
	@isTest(SeeAllData=true)
	private static void testCreateProjectDeliverablesFromIntercompanyProject() {
		// create test data
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Account testAccount = TestUtil.createAccount();
		Opportunity testMainOppty = TestUtil.createOpportunity(testAccount.id);
		tenrox__c testMainProject = TestUtil.generateProject();
		testMainProject.Opportunity__c = testMainOppty.id;
		insert testMainProject;
 		Opportunity testInterCompanyOppty = TestUtil.generateOpportunity(testAccount.id);
 		testInterCompanyOppty.Pricebook2Id = productHelper.pricebook.id;
 		insert testInterCompanyOppty;
 		OpportunityLineItem testLineItem = TestUtilProductHelper.createOpportunityLineItem(
 			  testInterCompanyOppty.id
 			, productHelper.testPricebookEntry.id
 		);
 		
 		// create intercompany project
 		Test.startTest();
 		tenrox__c testInterCompanyProject = TestUtil.generateProject();
 		testInterCompanyProject.opportunity__c = testInterCompanyOppty.id;
 		testInterCompanyProject.Customer_main_Project__c = testMainProject.id;
 		insert testInterCompanyProject;
 		Test.stopTest();
 		
 		// valdiate project deliverables created with expected fields populated
 		List<Project_Deliverable__c> deliverables = [
 			select amount__c, customer_main_project__c, line_description__c, product__c, quantity__c
 			from Project_Deliverable__c
 			where project__c = :testInterCompanyProject.id
 		]; 
 		system.assertEquals(1, deliverables.size());
 		Project_Deliverable__c deliverable = deliverables[0];
 		system.assertEquals(testMainProject.id, deliverable.customer_main_project__c);
	}
	
	@isTest(SeeAllData=true)
	private static void testCreateProjectDeliverablesFromProject() {
		// create test data
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Account testAccount = TestUtil.createAccount();
		Opportunity testOppty = TestUtil.generateOpportunity(testAccount.id);
		testOppty.pricebook2Id = productHelper.pricebook.id;
		insert testOppty;
		OpportunityLineItem testLineItem = TestUtilProductHelper.generateOpportunityLineItem(
 			  testOppty.id
 			, productHelper.testPricebookEntry.id
 		);
 		testLineItem.description = TestUtil.TEST_STRING;
 		insert testLineItem;
 		
 		// create intercompany project
 		Test.startTest();
 		tenrox__c testProject = TestUtil.generateProject();
		testProject.Opportunity__c = testOppty.id;
		insert testProject;
 		Test.stopTest();
 		
 		// valdiate project deliverables created with expected fields populated
 		List<Project_Deliverable__c> deliverables = [
 			select amount__c, line_description__c, product__c, quantity__c
 			from Project_Deliverable__c
 			where project__c = :testProject.id
 		]; 
 		system.assertEquals(1, deliverables.size());
 		Project_Deliverable__c deliverable = deliverables[0];
 		system.assertEquals(testLineItem.totalPrice, deliverable.amount__c);
 		system.assertEquals(testLineItem.description, deliverable.line_description__c);
 		system.assertEquals(productHelper.testProduct.id, deliverable.product__c);
 		system.assertEquals(testLineItem.quantity, deliverable.quantity__c);
	}

}