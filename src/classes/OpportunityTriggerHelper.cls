/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
    Place all basic opportunity trigger functions in this method to allow for cleaner
    trigger code
Change Log:
03/23/14 - Added hid_PO_Approval_Substatus__c to update Project_Substatus__c -Brittany Tankhim
*/
public class OpportunityTriggerHelper {

    /*
        Description:
            Pushes changes to Opportunity PO_Approval_Status__c into the related project's
            (by the tenrox__c field) Project_Status_CC__c, Project_Substatus__c,  field
    */
    public static void syncProjectStatus(List<Opportunity> newList, Map<Id, Opportunity> oldMap) {
        
        List<tenrox__c> updateProjects = new List<tenrox__c>();
        for(Opportunity newOppty : newList) {
            Opportunity oldOppty = oldMap.get(newOppty.id);
            
            if(newOppty.project__c != null && (
                (newOppty.PO_Approval_Status__c != oldOppty.PO_Approval_Status__c) || 
                (newOppty.hid_PO_Approval_Substatus__c != oldOppty.hid_PO_Approval_Substatus__c)
                )
              )
            {
              updateProjects.add(
                    new tenrox__c(
                          id = newOppty.project__c
                        , project_status_cc__c = newOppty.PO_Approval_Status__c
                        , Project_Substatus__c = newOppty.hid_PO_Approval_Substatus__c 
                    )
                );
            }
            
        }
        update updateProjects;
    }
    
    /* Test Methods */
    
    @isTest
    private static void syncProjectStatusTest() {
        // setup test data
        String newStatus = TestUtil.generateRandomString();
        tenrox__c testProject = TestUtil.createProject();
        Opportunity testOpportunity = TestUtil.createOpportunity(null);
        testOpportunity.Project__c = testProject.id; // validation rules requires project__c only be set on update
        update testOpportunity;
        
        // update opportunity po approval status
        Test.startTest();
        testOpportunity.PO_Approval_Status__c = newStatus;
        testOpportunity.hid_PO_Approval_Substatus__c  = newStatus;
        update testOpportunity;
        Test.stopTest();
        
        // validate related project status has changed
        testProject = [select project_status_cc__c, Project_Substatus__c from tenrox__c where id = :testProject.id];
        system.assertEquals(newStatus, testProject.project_status_cc__c);
        system.assertEquals(newStatus, testProject.Project_Substatus__c);
    }
}