public class VisualforceHelper {

	public static void addErrorMessage(String message) {
		addMessage(ApexPages.Severity.Error, message);
	}
	
	public static void addInfoMessage(String message) {
		addMessage(ApexPages.Severity.Info, message);
	}

	public static void addMessage(ApexPages.Severity severity, String message) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
	}

	@isTest
	private static void testErrorMessageHelpers() {
		addErrorMessage('asdf');
		system.assert(Apexpages.hasMessages(ApexPages.Severity.ERROR));
	}
	
	@isTest
	private static void testInfoMessgageHelpers() {
		addInfoMessage('adsd');
		system.assert(ApexPages.hasMessages(ApexPages.Severity.INFO));
	}

}