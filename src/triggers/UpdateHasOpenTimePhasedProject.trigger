trigger UpdateHasOpenTimePhasedProject on tenrox__c (after update) {

Map<Id, String> accountMap = new Map<Id, String>();

for (Integer i=0; i<Trigger.new.size();i++)
{
    String newRevenueDeliveryType = Trigger.new[i].Revenue_Delivery_Type__c;
    String oldRevenueDeliveryType = Trigger.old[i].Revenue_Delivery_Type__c;
    
    if (newRevenueDeliveryType == 'Time-phased' && newRevenueDeliveryType != oldRevenueDeliveryType && Trigger.new[i].Account__c != null)
    {
        accountMap.put(Trigger.new[i].Account__c, '' );
    }  
}
    Boolean empty = accountMap.isEmpty();
    
    if (!empty)
    {
        Set <Id> accountIdSet = new Set<Id>(); 
        accountIdSet = accountMap.keySet();

        Account[] accounts = [Select Id, Has_open_Time_Phased_project__c from Account where Id in: accountIdSet];
        
        for (Account acc : accounts)
        {
            acc.Has_open_Time_Phased_project__c = true;
        }
        update accounts;
    }
}