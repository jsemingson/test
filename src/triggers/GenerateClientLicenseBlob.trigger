trigger GenerateClientLicenseBlob on License_Key__c (before insert, before update) {

for (Integer i=0; i<Trigger.new.size();i++)
{
    Double License_Count_Legacy = Trigger.new[i].License_Count_Legacy__c;
    Double Lower_Bound_for_Warning_Msg_Legacy = Trigger.new[i].Lower_Bound_for_Warning_Msg_Legacy__c;
    Double Upper_Bound_for_Issue_Cert_Legacy = Trigger.new[i].Upper_Bound_for_Issue_Cert_Legacy__c;
    
    Double License_Count_Unknown = Trigger.new[i].License_Count_Unknown__c;
    Double Lower_Bound_for_Warning_Msg_Unknown = Trigger.new[i].Lower_Bound_for_Warning_Msg_Unknown__c;
    Double Upper_Bound_for_Issue_Cert_Unknown = Trigger.new[i].Upper_Bound_for_Issue_Cert_Unknown__c;
    
    Double License_Count_IPTV_STB = Trigger.new[i].License_Count_IPTV_STB__c;
    Double Lower_Bound_for_Warning_Msg_IPTV_STB = Trigger.new[i].Lower_Bound_for_Warning_Msg_IPTV_STB__c;
    Double Upper_Bound_for_Issue_Cert_IPTV_STB = Trigger.new[i].Upper_Bound_for_Issue_Cert_IPTV_STB__c;
    
    Double License_Count_Hybrid_STB = Trigger.new[i].License_Count_Hybrid_STB__c;
    Double Lower_Bound_for_Warning_Msg_Hybrid = Trigger.new[i].Lower_Bound_for_Warning_Msg_Hybrid__c;
    Double Upper_Bound_for_Issue_Cert_Hybrid = Trigger.new[i].Upper_Bound_for_Issue_Cert_Hybrid__c;
    
    Double License_Count_DVB_STB_w_SC = Trigger.new[i].License_Count_DVB_STB_w_SC__c;
    Double Lower_Bound_for_Warning_Msg_DVB_SC = Trigger.new[i].Lower_Bound_for_Warning_Msg_DVB_SC__c;
    Double Upper_Bound_for_Issue_Cert_DVB_SC = Trigger.new[i].Upper_Bound_for_Issue_Cert_DVB_SC__c;
    
    Double License_Count_DVB_STB_w_o_SC = Trigger.new[i].License_Count_DVB_STB_w_o_SC__c;
    Double Lower_Bound_for_Warning_Msg_DVB_nSC = Trigger.new[i].Lower_Bound_for_Warning_Msg_DVB_nSC__c;
    Double Upper_Bound_for_Issue_Cert_DVB_nSC = Trigger.new[i].Upper_Bound_for_Issue_Cert_DVB_nSC__c;
    
    Double License_Count_Desktop_PC = Trigger.new[i].License_Count_Desktop_PC__c;
    Double Lower_Bound_for_Warning_Msg_Dsktp_PC = Trigger.new[i].Lower_Bound_for_Warning_Msg_Dsktp_PC__c;
    Double Upper_Bound_for_Issue_Cert_Dsktp_PC = Trigger.new[i].Upper_Bound_for_Issue_Cert_Dsktp_PC__c;
    
    Double License_Count_Desktop_Mac = Trigger.new[i].License_Count_Desktop_Mac__c;
    Double Lower_Bound_for_Warning_Msg_DsktpMac = Trigger.new[i].Lower_Bound_for_Warning_Msg_DsktpMac__c;
    Double Upper_Bound_for_Issue_Cert_Dsktp_Mac = Trigger.new[i].Upper_Bound_for_Issue_Cert_Dsktp_Mac__c;
    
    Double License_Count_Web_PC = Trigger.new[i].License_Count_Web_PC__c;
    Double Lower_Bound_for_Warning_Msg_Web_PC = Trigger.new[i].Lower_Bound_for_Warning_Msg_Web_PC__c;
    Double Upper_Bound_for_Issue_Cert_Web_PC = Trigger.new[i].Upper_Bound_for_Issue_Cert_Web_PC__c;
    
    Double License_Count_Web_Mac = Trigger.new[i].License_Count_Web_Mac__c;
    Double Lower_Bound_for_Warning_Msg_Web_Mac = Trigger.new[i].Lower_Bound_for_Warning_Msg_Web_Mac__c;
    Double Upper_Bound_for_Issue_Cert_Web_Mac = Trigger.new[i].Upper_Bound_for_Issue_Cert_Web_Mac__c;
    
    Double License_Count_Web_iPhone = Trigger.new[i].License_Count_Web_iPhone__c;
    Double Lower_Bound_for_Warning_Msg_iPhone = Trigger.new[i].Lower_Bound_for_Warning_Msg_iPhone__c;
    Double Upper_Bound_for_Issue_Cert_iPhone = Trigger.new[i].Upper_Bound_for_Issue_Cert_iPhone__c;
    
    Double License_Count_Web_Android = Trigger.new[i].License_Count_Web_Android__c;
    Double Lower_Bound_for_Warning_Msg_Android = Trigger.new[i].Lower_Bound_for_Warning_Msg_Android__c;
    Double Upper_Bound_for_Issue_Cert_Android = Trigger.new[i].Upper_Bound_for_Issue_Cert_Android__c;
    
    Double License_Count_Web_STB = Trigger.new[i].License_Count_Web_STB__c;
    Double Lower_Bound_for_Warning_Msg_Web_STB = Trigger.new[i].Lower_Bound_for_Warning_Msg_Web_STB__c ;
    Double Upper_Bound_for_Issue_Cert_Web_STB = Trigger.new[i].Upper_Bound_for_Issue_Cert_Web_STB__c;
    
    Double License_Count_Group_1 = Trigger.new[i].License_Count_Group_1__c;
    Double Lower_Bound_for_Warning_Msg_Group_1 = Trigger.new[i].Lower_Bound_for_Warning_Msg_Group_1__c;
    Double Upper_Bound_for_Issue_Cert_Group_1= Trigger.new[i].Upper_Bound_for_Issue_Cert_Group_1__c;
    
    Double License_Count_Group_2 = Trigger.new[i].License_Count_Group_2__c;
    Double Lower_Bound_for_Warning_Msg_Group_2 = Trigger.new[i].Lower_Bound_for_Warning_Msg_Group_2__c;
    Double Upper_Bound_for_Issue_Cert_Group_2 = Trigger.new[i].Upper_Bound_for_Issue_Cert_Group_2__c;

    Double License_Count_Group_3 = Trigger.new[i].License_Count_Group_3__c;
    Double Lower_Bound_for_Warning_Msg_Group_3 = Trigger.new[i].Lower_Bound_for_Warning_Msg_Group_3__c;
    Double Upper_Bound_for_Issue_Cert_Group_3 = Trigger.new[i].Upper_Bound_for_Issue_Cert_Group_3__c;
        
    /* Delete after 10/01/13: Replaced by custom clone button that validates fields by changing the Record Type to "Build" - BT(8/14/13)
    if ( License_Count_Group_1 == null) License_Count_Group_1 = 0;
    if (Lower_Bound_for_Warning_Msg_Group_1 == null) Lower_Bound_for_Warning_Msg_Group_1 = 90;
    if (Upper_Bound_for_Issue_Cert_Group_1 == null) Upper_Bound_for_Issue_Cert_Group_1 = 110;
    if (License_Count_Group_2 == null) License_Count_Group_2 = 0;
    if (Lower_Bound_for_Warning_Msg_Group_2 == null) Lower_Bound_for_Warning_Msg_Group_2 = 90;
    if (Upper_Bound_for_Issue_Cert_Group_2 == null) Upper_Bound_for_Issue_Cert_Group_2 = 110;
    */
    
    //Set Group 1 with devices if not null
    String Group_1_Devices = Trigger.new[i].Group_1_Devices_for_Trigger__c;
    System.debug('Group_1_Devices =' +Group_1_Devices);
    if (Group_1_Devices != null)
    {
        Group_1_Devices ='CTIDGROUP.CTIDCLASS.Group1= '+ Group_1_Devices +'%';
    }
    else
    {
        Group_1_Devices = '';
    }
    System.debug('Group_1_Devices after if else statement =' +Group_1_Devices);
    
    //Set Group 2 with devices if not null
    String Group_2_Devices = Trigger.new[i].Group_2_Devices_for_Trigger__c;
    System.debug('Group_2_Devices =' +Group_2_Devices); 
        
    if (Group_2_Devices != null)
    {
        Group_2_Devices = 'CTIDGROUP.CTIDCLASS.Group2= '+ Group_2_Devices + '%';
    } 
    else
    {
        Group_2_Devices = '';
    }
     System.debug('Group_2_Devices after if else statement =' +Group_2_Devices);
     
    //Set Group 3 with devices if not null
    String Group_3_Devices = Trigger.new[i].Group_3_Devices_for_Trigger__c;
    System.debug('Group_3_Devices =' +Group_3_Devices); 
        
    if (Group_3_Devices != null)
    {
        Group_3_Devices = 'CTIDGROUP.CTIDCLASS.Group3= '+ Group_3_Devices + '%';
    } 
    else
    {
        Group_3_Devices = '';
    }
    System.debug('Group_3_Devices after if else statement =' +Group_3_Devices);
     
     
Trigger.new[i].Client_Licenses_Blob__c = 'CTIDCLASS.IPTV.legacy = '+(Integer)License_Count_Legacy +','+(Integer)( Lower_Bound_for_Warning_Msg_Legacy *License_Count_Legacy/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Legacy*License_Count_Legacy /100)+',Legacy Client Devices'+'%'+ 

'CTIDCLASS.IPTV.unknown = '+(Integer)License_Count_Unknown +','+(Integer)( Lower_Bound_for_Warning_Msg_Unknown *License_Count_Unknown/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Unknown*License_Count_Unknown /100)+',Unknown Devices'+'%'+

'CTIDCLASS.IPTV.stb_iptv = '+(Integer)License_Count_IPTV_STB +','+(Integer)( Lower_Bound_for_Warning_Msg_IPTV_STB *License_Count_IPTV_STB/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_IPTV_STB*License_Count_IPTV_STB /100)+',ViewRight STB for IPTV'+'%'+ 

'CTIDCLASS.IPTV.stb_hybrid = '+(Integer)License_Count_Hybrid_STB +','+(Integer)( Lower_Bound_for_Warning_Msg_Hybrid *License_Count_Hybrid_STB/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Hybrid*License_Count_Hybrid_STB /100)+',ViewRight STB for Hybrid'+'%'+ 

'CTIDCLASS.IPTV.stb_dvb_sc = '+(Integer)License_Count_DVB_STB_w_SC +','+(Integer)( Lower_Bound_for_Warning_Msg_DVB_SC *License_Count_DVB_STB_w_SC/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_DVB_SC*License_Count_DVB_STB_w_SC /100)+',ViewRight STB for DVB with Smartcard'+'%'+

'CTIDCLASS.IPTV.stb_dvb_nsc = '+(Integer)License_Count_DVB_STB_w_o_SC +','+(Integer)(Lower_Bound_for_Warning_Msg_DVB_nSC *License_Count_DVB_STB_w_o_SC/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_DVB_nSC*License_Count_DVB_STB_w_o_SC/100)+',ViewRight STB for DVB Smartcard-less'+'%'+ 

'CTIDCLASS.IPTV.desktop_pc = '+(Integer)License_Count_Desktop_PC +','+(Integer)( Lower_Bound_for_Warning_Msg_Dsktp_PC *License_Count_Desktop_PC/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Dsktp_PC*License_Count_Desktop_PC/100 )+',ViewRight Desktop for PC'+'%'+ 

'CTIDCLASS.IPTV.desktop_mac = '+(Integer)License_Count_Desktop_Mac +','+(Integer)( Lower_Bound_for_Warning_Msg_DsktpMac *License_Count_Desktop_Mac/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Dsktp_Mac*License_Count_Desktop_Mac /100)+',ViewRight Desktop for MAC'+'%'+

'CTIDCLASS.InternetTV.web_pc = '+(Integer)License_Count_Web_PC +','+(Integer)( Lower_Bound_for_Warning_Msg_Web_PC *License_Count_Web_PC/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Web_PC*License_Count_Web_PC /100)+',ViewRight Web for PC'+'%'+ 

'CTIDCLASS.InternetTV.web_mac= '+(Integer)License_Count_Web_Mac +','+(Integer)( Lower_Bound_for_Warning_Msg_Web_Mac *License_Count_Web_Mac/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Web_Mac*License_Count_Web_Mac /100)+',ViewRight Web for MAC'+'%'+ 

'CTIDCLASS.InternetTV.web_iphone= '+(Integer)License_Count_Web_iPhone +','+(Integer)( Lower_Bound_for_Warning_Msg_iPhone *License_Count_Web_iPhone/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_iPhone*License_Count_Web_iPhone/100 )+',ViewRight Web for iPhone'+'%'+

'CTIDCLASS.InternetTV.web_android= '+(Integer)License_Count_Web_Android +','+(Integer)( Lower_Bound_for_Warning_Msg_Android *License_Count_Web_Android/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Android*License_Count_Web_Android/100 )+',ViewRight Web for Android'+'%'+ 

'CTIDCLASS.InternetTV.web_stb= '+(Integer)License_Count_Web_STB +','+(Integer)( Lower_Bound_for_Warning_Msg_Web_STB *License_Count_Web_STB/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Web_STB*License_Count_Web_STB/100 )+',ViewRight Web for STB'+'%'+

'CTIDGROUP.Group1= '+(Integer)License_Count_Group_1+','+(Integer)( Lower_Bound_for_Warning_Msg_Group_1*License_Count_Group_1/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Group_1*License_Count_Group_1/100 )+',Group 1'+'%'+ Group_1_Devices +

'CTIDGROUP.Group2= '+(Integer)License_Count_Group_2+','+(Integer)( Lower_Bound_for_Warning_Msg_Group_2*License_Count_Group_2/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Group_2*License_Count_Group_2/100 )+',Group 2'+'%'+ Group_2_Devices +

'CTIDGROUP.Group3= '+(Integer)License_Count_Group_3+','+(Integer)( Lower_Bound_for_Warning_Msg_Group_3*License_Count_Group_3/100)+','+(Integer)( Upper_Bound_for_Issue_Cert_Group_3*License_Count_Group_3/100 )+',Group 3'+'%'+ Group_3_Devices ;
}
}