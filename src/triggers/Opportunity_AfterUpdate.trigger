/*
Developer: Ralph Callaway <ralphwcallaway@gmail.com>

Description:
Place all OBJECT OPERATION trigger actions in this file to provide
to allow for easy deduction of execution order 
*/
trigger Opportunity_AfterUpdate on Opportunity (after update) {

	// sync po approval and project status
	OpportunityTriggerHelper.syncProjectStatus(trigger.new, trigger.oldMap);
	
}