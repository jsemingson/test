trigger trg_invoice_after_ins_upd_del on AVSFQB__Invoices__c (after Insert, after Update, after Delete) 
{
    String [] OpptyIds;
    if (System.Trigger.IsInsert || System.Trigger.IsUpdate)
    {
        OpptyIds = new String[Trigger.new.size()];
        Integer iCount = 0;
            
        for (Integer i = 0; i < Trigger.new.size(); i++)  
        { 
            system.debug('Opportunity: ' + Trigger.new[i].AVSFQB__Opportunity__c);
            if (Trigger.new[i].AVSFQB__Opportunity__c != null)
            {
                OpptyIds[iCount] = Trigger.new[i].AVSFQB__Opportunity__c;
            }
        }
        }
    else if (System.Trigger.IsDelete)
    {
        OpptyIds = new String[Trigger.old.size()];
        Integer iCount = 0;
            
        for (Integer i = 0; i < Trigger.old.size(); i++)  
        { 
            system.debug('Opportunity: ' + Trigger.old[i].AVSFQB__Opportunity__c);
            if (Trigger.old[i].AVSFQB__Opportunity__c != null)
            {
                OpptyIds[iCount] = Trigger.old[i].AVSFQB__Opportunity__c;
            }
        }
    }

        
    if (OpptyIds != null && OpptyIds.size() > 0)
    {
        Double bookingAmt = 0;
        Double boookingPayments = 0;
        Double bookingBalance = 0;
        String oppId = '';
        Date latestPaymentDate = null;
        Double bookingDSO = 0;
        List<Opportunity> updOpps = new List<Opportunity>();
        
        for ( AVSFQB__Invoices__c invoiceRec : [Select Id, Name, AVSFQB__Opportunity__c, AVSFQB__Total_Invoice_Amount__c, AVSFQB__Payments__c, AVSFQB__Balance__c, Paid_Date__c, DSO_Weighted_Avg__c FROM AVSFQB__Invoices__c WHERE AVSFQB__Opportunity__c in :OpptyIds Order by Paid_Date__c desc ] )
        {
            if (oppId == '')
            {   
                bookingAmt = invoiceRec.AVSFQB__Total_Invoice_Amount__c;
                boookingPayments = invoiceRec.AVSFQB__Payments__c;
                bookingBalance = invoiceRec.AVSFQB__Balance__c;
                bookingDSO = invoiceRec.DSO_Weighted_Avg__c;
                if (latestPaymentDate == null && invoiceRec.Paid_Date__c != null)
                {
                    latestPaymentDate = invoiceRec.Paid_Date__c;
                }
                oppId = invoiceRec.AVSFQB__Opportunity__c;
            }
            else
            {
                if (oppId == invoiceRec.AVSFQB__Opportunity__c)
                {
                    bookingAmt =   bookingAmt + invoiceRec.AVSFQB__Total_Invoice_Amount__c;
                    boookingPayments =   boookingPayments + invoiceRec.AVSFQB__Payments__c;
                    bookingBalance =   bookingBalance + invoiceRec.AVSFQB__Balance__c;
                    bookingDSO = bookingDSO + invoiceRec.DSO_Weighted_Avg__c;
                    if (latestPaymentDate == null && invoiceRec.Paid_Date__c != null)
                    {
                        latestPaymentDate = invoiceRec.Paid_Date__c;
                    }
                }
                else
                {
                    
                    Opportunity OpportunityRec = [Select Id, Booking_Amount__c, Booking_Payments_Received__c, Booking_Balance__c, Last_Payment_Date__c From Opportunity Where Id = :oppId];
                    System.debug(oppId);
                    System.debug(OpportunityRec);
                    OpportunityRec.Booking_Amount__c = bookingAmt;
                    OpportunityRec.Booking_Payments_Received__c = boookingPayments;
                    OpportunityRec.Booking_Balance__c = bookingBalance;
                    OpportunityRec.Days_Sales_Outstanding__c = bookingDSO;
                    OpportunityRec.Last_Payment_Date__c = latestPaymentDate;
                    
                    updOpps.add(OpportunityRec);

                    bookingAmt = invoiceRec.AVSFQB__Total_Invoice_Amount__c;
                    boookingPayments = invoiceRec.AVSFQB__Payments__c;
                    bookingBalance = invoiceRec.AVSFQB__Balance__c;
                    bookingDSO = invoiceRec.DSO_Weighted_Avg__c;
                    latestPaymentDate = invoiceRec.Paid_Date__c;
                    oppId = invoiceRec.AVSFQB__Opportunity__c;
                }
            }
        }
        if (oppId != '')
        {
            Opportunity OpportunityRec = [Select Id, Booking_Amount__c, Booking_Payments_Received__c, Booking_Balance__c, Last_Payment_Date__c From Opportunity Where Id = :oppId];
            System.debug(oppId);
            System.debug(OpportunityRec);
            OpportunityRec.Booking_Amount__c = bookingAmt;
            OpportunityRec.Booking_Payments_Received__c = boookingPayments;
            OpportunityRec.Booking_Balance__c = bookingBalance;
            OpportunityRec.Days_Sales_Outstanding__c = bookingDSO;
            OpportunityRec.Last_Payment_Date__c = latestPaymentDate;
            updOpps.add(OpportunityRec);
            update(updOpps);
        }
        else if (System.Trigger.IsDelete)
        {
            oppId = OpptyIds[0];
            
            Opportunity OpportunityRec = [Select Id, Booking_Amount__c, Booking_Payments_Received__c, Booking_Balance__c, Last_Payment_Date__c From Opportunity Where Id = :oppId];

            OpportunityRec.Booking_Amount__c = 0.00;
            OpportunityRec.Booking_Payments_Received__c = 0.00;
            OpportunityRec.Booking_Balance__c = 0.00;
            OpportunityRec.Days_Sales_Outstanding__c = 0.00;
            OpportunityRec.Last_Payment_Date__c = null;
            
            updOpps.add(OpportunityRec);
            update(updOpps);
        }
    }
}