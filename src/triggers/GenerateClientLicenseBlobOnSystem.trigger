trigger GenerateClientLicenseBlobOnSystem on System__c (before insert, before update) {
/*
for (Integer i=0; i<Trigger.new.size();i++)
{
    Double License_Count_Legacy = Trigger.new[i].LicenseCountLegacy__c;
    Double License_Count_Unknown = Trigger.new[i].LicenseCountUnknown__c;    
    Double License_Count_IPTV_STB = Trigger.new[i].LicenseCountIPTVSTB__c;    
    Double License_Count_Hybrid_STB = Trigger.new[i].LicenseCountHybridSTB__c;
    Double License_Count_DVB_STB_w_SC = Trigger.new[i].LicenseCountDVBSTBwSC__c;    
    Double License_Count_DVB_STB_w_o_SC = Trigger.new[i].LicenseCountDVBSTBwoSC__c;    
    Double License_Count_Desktop_PC = Trigger.new[i].LicenseCountDesktopPC__c;    
    Double License_Count_Desktop_Mac = Trigger.new[i].LicenseCountDesktopMac__c;    
    Double License_Count_Web_PC = Trigger.new[i].LicenseCountWebPC__c;    
    Double License_Count_Web_Mac = Trigger.new[i].LicenseCountWebMac__c;    
    Double License_Count_Web_iPhone = Trigger.new[i].LicenseCountWebiPhone__c;    
    Double License_Count_Web_Android = Trigger.new[i].LicenseCountWebAndroid__c;    
    Double License_Count_Web_STB = Trigger.new[i].LicenseCountWebSTB__c;  
    Double License_Count_Group_1 = Trigger.new[i].LicenseCountGroup1__c;
    Double License_Count_Group_2 = Trigger.new[i].LicenseCountGroup2__c;
    //Double License_Count_Group_3 = Trigger.new[i].LicenseCountGroup3__c;
    
    Double LowerBoundforWarningMsg = Trigger.new[i].LowerBoundforWarningMsg__c;
    Double UpperBoundforIssueCert = Trigger.new[i].UpperBoundforIssueCert__c;
        
        
    //Set Group 1 with devices if not null    
    String Group_1_Devices = Trigger.new[i].Devices_in_Group_1_TRIGGER__c;
    System.debug('Group_1_Devices =' +Group_1_Devices);
    if (Group_1_Devices != null)
    {
        Group_1_Devices ='CTIDGROUP.CTIDCLASS.Group1= '+ Group_1_Devices +'%';
    }
    else
    {
        Group_1_Devices = '';
    }
    System.debug('Group_1_Devices after if else statement =' +Group_1_Devices);
    
    
    //Set Group 2 with devices if not null
    String Group_2_Devices = Trigger.new[i].Devices_in_Group_2_TRIGGER__c;
    System.debug('Group_2_Devices =' +Group_2_Devices); 
    if (Group_2_Devices != null)
    {
        Group_2_Devices = 'CTIDGROUP.CTIDCLASS.Group2= '+ Group_2_Devices + '%';
    } 
    else
    {
        Group_2_Devices = '';
    }
     System.debug('Group_2_Devices after if else statement =' +Group_2_Devices);
     
     
    //Set Group 3 with devices if not null
    String Group_3_Devices = Trigger.new[i].Devices_in_Group_3_TRIGGER__c;
    System.debug('Group_3_Devices =' +Group_3_Devices); 
        
    if (Group_3_Devices != null)
    {
        Group_3_Devices = 'CTIDGROUP.CTIDCLASS.Group3= '+ Group_3_Devices + '%';
    } 
    else
    {
        Group_3_Devices = '';
    }
    System.debug('Group_3_Devices after if else statement =' +Group_3_Devices);
    
    
Trigger.new[i].Client_Licenses_Blob__c = 'CTIDCLASS.IPTV.legacy = '+(Integer)License_Count_Legacy +','+(Integer)( LowerBoundforWarningMsg *License_Count_Legacy/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Legacy /100)+',Legacy Client Devices'+'%'+ 

'CTIDCLASS.IPTV.unknown = '+(Integer)License_Count_Unknown +','+(Integer)( LowerBoundforWarningMsg *License_Count_Unknown/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Unknown /100)+',Unknown Devices'+'%'+

'CTIDCLASS.IPTV.stb_iptv = '+(Integer)License_Count_IPTV_STB +','+(Integer)( LowerBoundforWarningMsg *License_Count_IPTV_STB/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_IPTV_STB /100)+',ViewRight STB for IPTV'+'%'+ 

'CTIDCLASS.IPTV.stb_hybrid = '+(Integer)License_Count_Hybrid_STB +','+(Integer)( LowerBoundforWarningMsg *License_Count_Hybrid_STB/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Hybrid_STB /100)+',ViewRight STB for Hybrid'+'%'+ 

'CTIDCLASS.IPTV.stb_dvb_sc = '+(Integer)License_Count_DVB_STB_w_SC +','+(Integer)( LowerBoundforWarningMsg *License_Count_DVB_STB_w_SC/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_DVB_STB_w_SC /100)+',ViewRight STB for DVB with Smartcard'+'%'+

'CTIDCLASS.IPTV.stb_dvb_nsc = '+(Integer)License_Count_DVB_STB_w_o_SC +','+(Integer)(LowerBoundforWarningMsg *License_Count_DVB_STB_w_o_SC/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_DVB_STB_w_o_SC/100)+',ViewRight STB for DVB Smartcard-less'+'%'+ 

'CTIDCLASS.IPTV.desktop_pc = '+(Integer)License_Count_Desktop_PC +','+(Integer)( LowerBoundforWarningMsg *License_Count_Desktop_PC/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Desktop_PC/100 )+',ViewRight Desktop for PC'+'%'+ 

'CTIDCLASS.IPTV.desktop_mac = '+(Integer)License_Count_Desktop_Mac +','+(Integer)( LowerBoundforWarningMsg *License_Count_Desktop_Mac/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Desktop_Mac /100)+',ViewRight Desktop for MAC'+'%'+

'CTIDCLASS.InternetTV.web_pc = '+(Integer)License_Count_Web_PC +','+(Integer)( LowerBoundforWarningMsg *License_Count_Web_PC/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Web_PC /100)+',ViewRight Web for PC'+'%'+ 

'CTIDCLASS.InternetTV.web_mac= '+(Integer)License_Count_Web_Mac +','+(Integer)( LowerBoundforWarningMsg *License_Count_Web_Mac/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Web_Mac /100)+',ViewRight Web for MAC'+'%'+ 

'CTIDCLASS.InternetTV.web_iphone= '+(Integer)License_Count_Web_iPhone +','+(Integer)( LowerBoundforWarningMsg *License_Count_Web_iPhone/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Web_iPhone/100 )+',ViewRight Web for iPhone'+'%'+

'CTIDCLASS.InternetTV.web_android= '+(Integer)License_Count_Web_Android +','+(Integer)( LowerBoundforWarningMsg *License_Count_Web_Android/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Web_Android/100 )+',ViewRight Web for Android'+'%'+ 

'CTIDCLASS.InternetTV.web_stb= '+(Integer)License_Count_Web_STB +','+(Integer)( LowerBoundforWarningMsg *License_Count_Web_STB/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Web_STB/100 )+',ViewRight Web for STB'+'%'+

'CTIDGROUP.Group1= '+(Integer)License_Count_Group_1+','+(Integer)( LowerBoundforWarningMsg*License_Count_Group_1/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Group_1/100 )+',Group 1'+'%'+ Group_1_Devices +

'CTIDGROUP.Group2= '+(Integer)License_Count_Group_2+','+(Integer)( LowerBoundforWarningMsg*License_Count_Group_2/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Group_2/100 )+',Group 2'+'%'+ Group_2_Devices +

'CTIDGROUP.Group3= '+(Integer)License_Count_Group_3+','+(Integer)( LowerBoundforWarningMsg*License_Count_Group_3/100)+','+(Integer)( UpperBoundforIssueCert*License_Count_Group_3/100 )+',Group 3'+'%'+ Group_3_Devices ;
}
*/
}