/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Place all OBJECT OPERATION trigger actions in this file to provide
	to allow for easy deduction of execution order 
*/
trigger Project_AfterInsert on tenrox__c (after insert) {

	// create project deliverables from parent opportuntiy line items
	Project_TriggerHelper.createProjectDeliverables(trigger.new);

}